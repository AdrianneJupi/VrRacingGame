extends Spatial

signal following_changed(fol)

export(String) var controller_side := "left"
export(Vector3) var controller_offset := Vector3(0, 0, 0.16)
export(float) var follow_trans_spd := 20.0
var controller_id
var controller : ARVRController

var following : Object = null setget set_following
var in_follow_trans := false
var trans_snap_distance := 0.01

var interactible : Object = null
var handle_offset : Vector3


func _ready():
	if controller_side == "left":
		controller = Global.left_oculus
		$Armature.scale = Vector3(-1, 1, 1)
	else: controller = Global.right_oculus
	controller_id = controller.controller_id
	handle_offset = (global_transform.origin - 
					$Armature/HandlePos.global_transform.origin)


func _process(delta):
	$AnimationHandler.update_animation()
	_update_movement(delta)
	Global.emit_signal("set_debug_info", 1, "GLOBAL_HAND_POS", global_transform.origin)


func _unhandled_input(event):
	var grip = controller_side + "_grip"
	if event.is_action(grip):
		if interactible != null:
			in_follow_trans = true
			if event.is_action_pressed(grip):
				if interactible.owner.request_grip(controller, controller_side):
					self.following = interactible
					Global.emit_signal("shake_controller", controller_id, 0.35, 0.022)
			else:
				if interactible.owner.release_grip(controller):
					Global.emit_signal("shake_controller", controller_id, 0.1, 0.015)
					self.following = null


func _update_movement(delta):
	var fl = controller if following == null else following
	var of = controller_offset if following == null else handle_offset
	var target_transform = fl.global_transform.translated(of)
	if in_follow_trans:
		var dis = global_transform.origin.distance_to(target_transform.origin)
		if  dis < trans_snap_distance:
			global_transform = target_transform
			in_follow_trans = false
		else:
			global_transform = global_transform.interpolate_with(
								target_transform, delta * follow_trans_spd)
	else:
		global_transform = target_transform


func _check_for_interactibles():
	var oa = $Armature/Interaction.get_overlapping_areas()
	if ! oa.empty():
		for area in oa:
			if _check_interaction_area(area):
				return true

func _check_interaction_area(area):
	if area.is_in_group("GRAB"):
		interactible = area
		return true


func _on_Interaction_area_exited(area):
	if area.is_in_group("GRAB"):
		if not _check_for_interactibles():
			interactible = null


func set_following(obj):
	following = obj
	emit_signal("following_changed", obj)
