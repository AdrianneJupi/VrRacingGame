extends Spatial

var max_size := 500

var is_scrolling := false
var is_menu_active := false

func _ready():
	$Screen.material_override.albedo_texture = $Viewport.get_texture()
	_update_text()
	Global.connect("set_debug_info", self, "update_info")

func _update_text():
	var f = File.new()
	var lab = $Viewport/Control/Label
	f.open("user://logs/log.txt", File.READ)
	lab.text = f.get_as_text()
	f.close()
	lab.rect_position.y = - lab.rect_size.y + max_size 

func _process(_delta):
	var scroll_down = Global.left_oculus.is_button_pressed(JOY_VR_TRIGGER)
	var scroll_up = Global.left_oculus.is_button_pressed(JOY_VR_GRIP)
	var scroll = 2 if (scroll_up and scroll_down) else (scroll_up - scroll_down)
	is_menu_active = Global.left_oculus.is_button_pressed(JOY_OCULUS_MENU)
	is_scrolling = is_menu_active and (scroll != 0)
	$Viewport/Control.visible = is_menu_active
	
	if is_scrolling:
		if scroll != 2:
			$Viewport/Control/Label.rect_position.y += (scroll * 9)

func _on_Timer_timeout():
	if is_menu_active and not is_scrolling:
		_update_text()


func update_info(slot, text, value):
	var target = get_node("Viewport/Info" + String(slot))
	if not target == null:
		target.text = text + ": " + str(value)
	else:
		print("ERROR: Invalid Debug Info Slot ", slot)
	
