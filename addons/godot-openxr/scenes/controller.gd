extends ARVRController

signal activated
signal deactivated


func _ready():
	Global.connect("shake_controller", self, "on_shake")

func _process(delta):
	if get_is_active():
		if !visible:
			visible = true
			print("Activated " + name)
			emit_signal("activated")
	elif visible:
		visible = false
		print("Deactivated " + name)
		emit_signal("deactivated")


func on_shake(id, strength, duration):
	if controller_id == id:
		rumble = strength
		$ShakeTimer.start(duration)


func _on_ShakeTimer_timeout():
	rumble = 0.0

