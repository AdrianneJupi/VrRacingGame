extends Node

# warning-ignore:unused_signal
signal set_debug_info(slot, text, value)
# warning-ignore:unused_signal
signal shake_controller(controller_id, strength, duration)
# warning-ignore:unused_signal
signal calibrate_origin()

var left_oculus : ARVRController
var right_oculus : ARVRController
var vr_origin : ARVROrigin
var vr_camera : ARVRCamera

func get_headset_height():
	return (vr_camera.global_transform.origin.y - 
			vr_origin.global_transform.origin.y)
