extends Spatial


export(bool) var on := false
export(float) var rotation_steps := 3.5
export(int) var threshold := 24

var state = 0
var direction
var max_rotation
var hand = 0


func _ready():
	_set_indicator()
	direction = -1 if on else 1
	max_rotation = abs($Switch.rotation_degrees.z)
	$Switch.rotation_degrees.z = - direction * max_rotation
	threshold = int(max_rotation - threshold)


func _physics_process(_delta):
	if state == 0:
		var ob = $Switch.get_overlapping_bodies()
		if not ob.empty():
			for body in ob:
				if body.is_in_group("HAND"):
					hand = body
					_move_away_from()
					break
		elif $Switch.rotation_degrees.z != -direction * max_rotation:
			_rotate_switch(rotation_steps * 1.2, -direction)


func _move_away_from():
	var rot = $Switch.rotation_degrees.z
	_rotate_switch(rotation_steps, direction)
	if (direction == 1 and rot > -threshold) or \
			(direction == -1 and rot < threshold):
		_flip_switch()



func _rotate_switch(amount, dir):
	var new_rot = $Switch.rotation_degrees.z + (amount * dir)
	$Switch.rotation_degrees.z = clamp(new_rot, -max_rotation, max_rotation)


func _flip_switch():
	$SwitchAnimation.interpolate_property($Switch, "rotation_degrees:z",
			$Switch.rotation_degrees.z, max_rotation * direction, 0.1,
			Tween.TRANS_BACK, Tween.EASE_OUT)
	$SwitchAnimation.start()
	on = !on
	_set_indicator()
	direction *= -1
	state = 1


func _set_indicator():
	var color = Color.green if on else Color.red
	$Base/Indicator.get_surface_material(0).albedo_color = color


func _on_SwitchAnimation_tween_all_completed():
	Global.emit_signal("shake_controller", hand.owner.controller_id, 0.3, 0.06)
	state = 0
