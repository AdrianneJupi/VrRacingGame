extends Spatial

signal handle_value_changed(side, new_value)
signal handle_grip_changed(side, held)

export(String) var hand_side := "left"
export(NodePath) var twin_path = null
var controller = null setget _grip_changed
var twin_handle
var cont_pos := Vector3.ZERO
var cont_prev_pos := Vector3.ZERO
var momentum : Vector3
var slip_factor := 0.82

onready var range_start = $Start.transform.origin
onready var range_end = $End.transform.origin
onready var range_distance = range_start.distance_to(range_end)
onready var value = _calculate_value()


func _ready():
	twin_handle = get_node(twin_path) if twin_path else null

func _physics_process(_delta):
	if controller != null or momentum != Vector3.ZERO:
		var movement
		if controller != null:
			_update_controller_vectors()
			movement = _get_controller_movement()
		else:
			_update_momentum()
			movement = momentum
		var new_or = $Handle.transform.translated(movement).origin
		$Handle.transform.origin = _clamp_transform(new_or, range_start, range_end)
		value = _calculate_value()
		Global.emit_signal("set_debug_info", 2, "HANDLE_VALUE", value)


func _update_controller_vectors(ini := false):
	var p = transform.xform_inv(controller.transform.origin)
	cont_prev_pos = cont_pos if not ini else p
	cont_pos = p


func _get_controller_movement():
	return cont_pos - cont_prev_pos


func _clamp_transform(trsf, start, end):
	var res := Vector3.ZERO
	for c in 3:
		res[c] = clamp(trsf[c], min(start[c], end[c]), max(start[c], end[c]))
	return res


func request_grip(cont: ARVRController, side: String):
	if controller == null:
		var check = twin_handle == null or twin_handle.controller == null
		if side == hand_side or check:
			self.controller = cont
			_update_controller_vectors(true)
			return true


func release_grip(cont: ARVRController):
	if controller == cont:
		self.controller = null
		momentum = _get_controller_movement()
		return true


func _update_momentum():
	momentum *= slip_factor
	momentum =  Vector3.ZERO if momentum.length() < 0.001 else momentum


func _calculate_value():
#	var dis = $Start.transform.origin.distance_to($Handle.transform.origin)
	var dis = range_end.distance_to($Handle.transform.origin)
	var new_val = dis / range_distance
	emit_signal("handle_value_changed", hand_side, new_val)
	return new_val


func _grip_changed(cont):
	controller = cont
	emit_signal("handle_grip_changed", hand_side, cont != null)
