extends Spatial

export(Curve) var yaw_curve
export(Curve) var speed_curve
export(Curve) var roll_curve
export(Curve) var cont_roll_dist_curve
export(Curve) var headset_roll_fact_cur
export(float) var yaw_roll_ratio := 0.4
export(float) var yaw_roll_peak := 50 #in degrees, when roll stops affecting yaw
export(float) var roll_max := 100 
export(float) var cont_roll_dist_max := 0.4 #in meters
export(float) var headset_max_roll := 35.0

var handles_values:= {"left": 0.0, "right": 0.0}
var handles_grips:= {"left": false, "right": false, "sum": "none"}
var conts_pos := {}
var speed := 0.0
var yaw := 0.0
var roll = 0.0
var roll_factor := 0.0


func _process(_delta):
	_update_conts_pos()
	if handles_grips["sum"] == "both":
		_calculate_roll()
	_calculate_yaw()


func _on_handle_value_changed(side, new_value):
	handles_values[side] = new_value
	_calculate_speed()


func _on_handle_grip_changed(side, new_value):
	handles_grips[side] = new_value
	match handles_grips.values().slice(0, 1):
		[true, true]: handles_grips["sum"] = "both"
		[false, false]: handles_grips["sum"] = "none"
		_: handles_grips["sum"] = "one"


func _calculate_speed():
	var reac := []
	for v in handles_values.values():
		reac.append(speed_curve.interpolate(v))
	speed = speed_curve.interpolate((reac[0] + reac[1]) / 2)


func _calculate_yaw():
	var hv = handles_values["right"] - handles_values["left"]
	var yf = abs(hv) * yaw_roll_ratio
	var rf = abs(roll_factor) * ((1 / yaw_roll_ratio) - 1)
	rf = 1 + rf if sign(hv) == sign(roll_factor) else 1
	yaw = sign(hv) * yaw_curve.interpolate(yf * rf)
	Global.emit_signal("set_debug_info", 4, "handles values", hv)
	Global.emit_signal("set_debug_info", 5, "roll factor", rf)
	Global.emit_signal("set_debug_info", 6, "yaw", yaw)


func _calculate_roll():
	var cont_angle = rad2deg(conts_pos["right"].angle_to_point(conts_pos["left"]))
	var dist_factor = conts_pos["right"].distance_to(conts_pos["left"])
	dist_factor = clamp(dist_factor, 0, cont_roll_dist_max) / cont_roll_dist_max
	dist_factor = cont_roll_dist_curve.interpolate(dist_factor)
	
	var headset_roll = Global.vr_camera.rotation_degrees.z
	var headset_factor = clamp(abs(headset_roll), 0, headset_max_roll) / headset_max_roll
	headset_factor *= 0 if sign(headset_roll) != sign(cont_angle) else 1
	headset_factor = headset_roll_fact_cur.interpolate(headset_factor)
	
	var cont_factor = clamp(abs(cont_angle), 0, roll_max) / roll_max
	cont_factor = roll_curve.interpolate(cont_factor)
	
	roll = ((dist_factor * cont_angle) * headset_factor) * cont_factor
	roll = deg2rad(clamp(roll, -roll_max, roll_max))
	roll_factor = clamp(rad2deg(roll) / yaw_roll_peak, -1, 1)
	
	Global.emit_signal("set_debug_info", 3, "roll", rad2deg(roll))



func _update_conts_pos():
	var base_trans = Global.vr_origin.transform
	var conts = {"left" : Global.left_oculus, "right" : Global.right_oculus}
	for c in conts:
		conts[c] = base_trans.xform_inv(conts[c].global_transform.origin)
		conts[c] = Vector2(conts[c].x, conts[c].y)
	conts_pos = conts.duplicate()

