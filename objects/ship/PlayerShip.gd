extends Spatial


var speed := {cur = 0.0, mul = 1.0, accel = 0.01, decel = 0.02}
var yaw := {cur = 0.0, tar = 0.0, mul = deg2rad(1.0), accel = 0.04}
var roll := {cur = 0.0, tar = 0.0, accel = 0.05}


func _ready():
	pass

func _input(event):
	if event.is_action_pressed("left_stick") :
		Global.emit_signal("calibrate_origin")
	if event.is_action_pressed("right_stick"):
		$Model/Wings.visible = !$Model/Wings.visible


func _process(_delta):
	_calculate_speed()
	_calculate_yaw()
	_calculate_roll()
	transform.basis = Basis()
	rotate_z(roll.cur)
	rotate_y(yaw.cur)
	
	$InputHandler/SpeedMeter.scale.x = 1 - speed.cur


func _calculate_speed():
	var tar = $InputHandler.speed * speed.mul
	var ap = speed.accel if speed.cur < tar else speed.decel
	speed.cur = Utility.approach(speed.cur, tar, ap)

func _calculate_yaw():
	yaw.tar += $InputHandler.yaw * yaw.mul
	yaw.cur = lerp(yaw.cur, yaw.tar, yaw.accel)

func _calculate_roll():
	roll.tar = $InputHandler.roll
	roll.cur = lerp(roll.cur, roll.tar, roll.accel)
