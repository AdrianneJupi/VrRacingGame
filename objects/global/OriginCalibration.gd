extends Spatial


func _ready():
	set_notify_transform(true)
	Global.connect("calibrate_origin", self, "_set_vr_origin")


func _notification(what):
	if what == NOTIFICATION_TRANSFORM_CHANGED:
		Global.vr_origin.global_transform = $OriginPosition.global_transform


func _set_vr_origin():
	ARVRServer.center_on_hmd(ARVRServer.RESET_BUT_KEEP_TILT, true)
	var h = Global.get_headset_height()
	var t = $CameraPosition.transform
	t.origin.y -= h
	$OriginPosition.transform = t 
	self._notification(NOTIFICATION_TRANSFORM_CHANGED)


func _on_CalibrateTimer_timeout():
	_set_vr_origin()
