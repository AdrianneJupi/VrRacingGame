extends Node

var anim_mode := "default"


func update_animation():
	var anim_tree = $AnimationTree
	var grip = owner.controller.get_joystick_axis(JOY_VR_ANALOG_GRIP)
	var trigger = owner.controller.get_joystick_axis(JOY_VR_ANALOG_TRIGGER)
	
	match anim_mode:
		"default":
				var index_anim = int(trigger == 0.0)
				var index_blend = grip if trigger == 0.0 else trigger
				_set_blend(anim_tree, "PalmBlend", grip)
				_set_blend(anim_tree, "IndexBlend", index_anim)
				_set_blend(anim_tree, "HandBlend", index_blend)
		"handle":
				grip = clamp(grip, 0.46, 0.86)
				_set_blend(anim_tree, "PalmBlend", grip)
				_set_blend(anim_tree, "HandBlend", grip)
				_set_blend(anim_tree, "IndexBlend", 0.0)


func _set_blend(tree, param, amount):
	tree.set("parameters/%s/blend_amount" % param, amount)


func _on_Hand_following_changed(fol):
	match typeof(fol):
		TYPE_NIL:
			anim_mode = "default"
		_:
			anim_mode = "handle"
